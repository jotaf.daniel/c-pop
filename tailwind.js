module.exports = {
  theme: {
    extend: {
      height: {
        "90": "90vh"
      },
      minHeight: {
        "64": "16rem"
      }
    }
  },
  variants: {},
  plugins: []
}
